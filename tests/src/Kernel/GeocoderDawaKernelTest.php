<?php

namespace Drupal\Tests\geocoder_dawa\Kernel;

use Drupal\geocoder\Entity\GeocoderProvider;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the DAWA geocoder provider plugin.
 */
class GeocoderDawaKernelTest extends KernelTestBase {

  /**
   * The address string we are testing with.
   */
  const TEST_ADDRESS_STRING = 'Suomisvej 2, 1927 Frederiksberg';

  /**
   * {@inheritDoc}
   */
  protected static $modules = ['geocoder', 'geocoder_dawa'];

  /**
   * Our test provider.
   *
   * @var \Drupal\geocoder\GeocoderProviderInterface
   */
  protected $provider;

  /**
   * The geocoder service.
   *
   * @var \Drupal\geocoder\Geocoder
   */
  protected $geocoder;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->provider = GeocoderProvider::create([
      'id' => 'dawa',
      'plugin' => 'dawa',
    ]);
    $this->provider->save();
    $this->geocoder = \Drupal::service('geocoder');
  }

  /**
   * Test that the correct latitude is returned when geocoding.
   */
  public function testGetLatitude() {
    $coordinates = $this->geocoder->geocode(self::TEST_ADDRESS_STRING, [
      $this->provider,
    ])->first()->getCoordinates();

    $this->assertEquals(55.67936222, $coordinates->getLatitude());
  }

  /**
   * Test that the correct longitude is returned when geocoding.
   */
  public function testGetLongitude() {
    $coordinates = $this->geocoder->geocode(self::TEST_ADDRESS_STRING, [
      $this->provider,
    ])->first()->getCoordinates();

    $this->assertEquals(12.55511495, $coordinates->getLongitude());
  }

  /**
   * Test that the correct street name is returned when reverse geocoding.
   */
  public function testReverseGeocodeStreetName() {
    $street_name = $this->geocoder->reverse(55.67936222, 12.55511495, [
      $this->provider,
    ])->first()->getStreetName();

    $this->assertEquals('Suomisvej', $street_name);
  }

}
