<?php

namespace Drupal\geocoder_dawa\Plugin\Geocoder\Provider;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\geocoder\ProviderBase;
use Geocoder\Model\Address;
use Geocoder\Model\AddressCollection;
use Geocoder\Model\AdminLevelCollection;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a DAWA geocoder provider plugin.
 *
 * @GeocoderProvider(
 *   id = "dawa",
 *   name = "DAWA",
 * )
 */
class Dawa extends ProviderBase {

  // The DAWA address washing endpoint.
  const DATAWASH_ENDPOINT = "https://api.dataforsyningen.dk/datavask/adresser";

  // The DAWA reverse geocoding endpoint. DAWA only supports reverse geocoding
  // for adgangsadresser.
  const REVERSE_ENDPOINT = "https://api.dataforsyningen.dk/adgangsadresser/reverse";

  /**
   * Constructs a DAWA Geocoder object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \GuzzleHttp\ClientInterface $client
   *   The http client.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigFactoryInterface $config_factory,
    CacheBackendInterface $cache_backend,
    LanguageManagerInterface $language_manager,
    protected ClientInterface $client,
    protected LoggerChannelFactory $loggerChannelFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $config_factory, $cache_backend, $language_manager);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('cache.geocoder'),
      $container->get('language_manager'),
      $container->get('http_client'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function doGeocode($source) {
    return $this->geocode($source);
  }

  /**
   * {@inheritDoc}
   */
  public function geocode($source) {
    $washed_address = $this->getDatawashedAddress($source);

    if ((!$washed_address) || (!$washed_address->href)) {
      $this->loggerChannelFactory->get('dawa geocoder')
        ->error('Error geocoding with DAWA: No washed address: ' . $source);
      return NULL;
    }

    try {
      $result = $this->client->get($washed_address->href);
    }
    catch (RequestException $e) {
      $this->loggerChannelFactory->get('dawa geocoder')
        ->error('Error geocoding with DAWA: ' . $e->getMessage());
      return NULL;
    }

    $address = json_decode($result->getBody()->getContents(), TRUE);
    $results[] = $this->getAddressFactory()->createFromArray([
      'providedBy' => $this->pluginId,
      'longitude' => $address['adgangsadresse']['adgangspunkt']['koordinater'][0],
      'latitude' => $address['adgangsadresse']['adgangspunkt']['koordinater'][1],
      'streetName' => $address['adgangsadresse']['vejstykke']['navn'],
      'streetNumber' => $address['adgangsadresse']['husnr'],
      'postalCode' => $address['adgangsadresse']['postnummer']['nr'],
      'locality' => $address['adgangsadresse']['postnummer']['navn'],
      'country' => 'Danmark',
      'countryCode' => 'DK',
    ]);

    return new AddressCollection($results);
  }

  /**
   * Returns the address factory.
   *
   * @return \Geocoder\Model\Address
   *   Return the address Factory.
   */
  protected function getAddressFactory(): Address {
    return new Address('dawa', new AdminLevelCollection());
  }

  /**
   * Get the datawashed address from DAWA.
   *
   * @param string $source
   *   The address string.
   *
   * @return bool|object
   *   False if the address can't be washed, or the address object.
   */
  protected function getDatawashedAddress(string $source) {
    // We remove the "DK" part of the $source string, as DAWA expects a danish
    // address. With the DK, we can't wash the address. DAWA gets confused.
    if (str_ends_with($source, 'DK')) {
      $source = substr($source, 0, -2);
    }

    try {
      $result = $this->client->get(self::DATAWASH_ENDPOINT, [
        'query' => [
          'betegnelse' => $source,
        ],
      ]);
    }
    catch (RequestException $e) {
      $this->loggerChannelFactory->get('dawa geocoder')
        ->error('Error datawashing with DAWA: ' . $e->getMessage());
      return FALSE;
    }

    $data = json_decode($result->getBody()->getContents());

    // Return the first result. The response from DAWA are sorted by the best
    // washed address first.
    if ($data->resultater[0]->adresse->href) {
      return $data->resultater[0]->adresse;
    }
    elseif ($data->resultater[0]->aktueladresse->href) {
      return $data->resultater[0]->aktueladresse;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function doReverse($latitude, $longitude) {
    return $this->reverse($latitude, $longitude);
  }

  /**
   * {@inheritDoc}
   */
  public function reverse($latitude, $longitude) {
    if (!$latitude || !$longitude) {
      $this->loggerChannelFactory->get('dawa geocoder')
        ->error('Error reverse geocoding with DAWA: No latitude or longitude.');

      return NULL;
    }

    try {
      $result = $this->client->get(self::REVERSE_ENDPOINT, [
        'query' => [
          'x' => $longitude,
          'y' => $latitude,
        ],
      ]);

      $address = json_decode($result->getBody()->getContents(), TRUE);

      return new AddressCollection([
        $this->getAddressFactory()->createFromArray([
          'providedBy' => $this->pluginId,
          'longitude' => $longitude,
          'latitude' => $latitude,
          'streetName' => $address['vejstykke']['navn'],
          'streetNumber' => $address['husnr'],
          'postalCode' => $address['postnummer']['nr'],
          'locality' => $address['postnummer']['navn'],
          'country' => 'Danmark',
          'countryCode' => 'DK',
        ]),
      ]);
    }
    catch (RequestException $e) {
      $this->loggerChannelFactory->get('dawa geocoder')
        ->error('Error reverse geocoding with DAWA: ' . $e->getMessage());

      return NULL;
    }
  }

}
