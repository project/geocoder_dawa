# Geocoder DAWA

A Geocoder provider using
DAWA - [Danmarks Adressers Web API](https://dawadocs.dataforsyningen.dk/).

This module adds a [Geocoder](https://www.drupal.org/project/geocoder) DAWA
provider.
It tries to datawash the given address, if any results, take the first one and
then
do the geocoding based on the washed
address.

### How to enable

Go to /admin/config/system/geocoder/geocoder-provider and add it from the
dropdown.
